$("#arivalDate").flatpickr({});
$("#arivalTime").flatpickr({
  enableTime: true,
  noCalendar: true,
  dateFormat: "H:i",
  time_24hr: true,
});
$("#returnDate").flatpickr({});
$("#returnTime").flatpickr({
  enableTime: true,
  noCalendar: true,
  dateFormat: "H:i",
  time_24hr: true,
});

// ***** //

var returnTrip = document.getElementById("returnTrip");

document.getElementById("toggleReturnTrip").addEventListener("change", function (event) {
  if (event.target.checked) {
    returnTrip.style.maxHeight = "1000px";
    returnTrip.style.overflow = "visible";
    addReturnTripToCart();
    returnTripWatcher.value = true;
  } else {
    returnTrip.style.maxHeight = "0";
    returnTrip.style.overflow = "hidden";
    document.getElementById("returnTripCart").remove();
    returnTripWatcher.value = false;
  }
  calculateTotalPrice();
});

function addReturnTripToCart() {
  let cart = document.querySelector(".cart__trips");
  let price = document.getElementById("returnTripPrice").textContent;
  let tripStartPoint = document.getElementById("tripStartPoint").textContent;
  let tripEndPoint = document.getElementById("tripEndPoint").textContent;

  cart.innerHTML += `
    <div id="returnTripCart" class="cart__item mt-3">
      <div class="d-flex justify-content-between">
        <b class="cart__point">${tripEndPoint}</b>
        <span class="text--gray"><span class="cart-item__price">${price}</span> USD</span>
      </div>
      <div class="mt-3">
        <b class="cart__point">${tripStartPoint}</b>
      </div>
    </div>
  `;
}

function increment(element) {
  let input = element.parentNode.querySelector("input");
  let spanValue = element.parentNode.querySelector(".input-value");
  let value = parseInt(input.value);
  let { max } = getMaxAndMinFromElement(input);
  if (value === max) return;
  input.value = value + 1;
  spanValue.textContent = input.value;
}

function decrement(element) {
  let input = element.parentNode.querySelector("input");
  let spanValue = element.parentNode.querySelector(".input-value");
  let value = parseInt(input.value);
  let { min } = getMaxAndMinFromElement(input);
  if (value === min) return;
  input.value = value - 1;
  spanValue.textContent = input.value;
}

function getMaxAndMinFromElement(element) {
  return {
    max: parseInt(element.getAttribute("max")),
    min: parseInt(element.getAttribute("min")),
  };
}

$(".additional-item").on("click", ".input-number__action", function (e) {
  let additionalItem = $(this).closest(".additional-item")[0];
  let id = additionalItem.querySelector("input").getAttribute("id");
  let count = additionalItem.querySelector("input").value;
  let name = additionalItem.querySelector("input").getAttribute("cart-name");
  let price = additionalItem.querySelector(".additional-item__hidden").textContent;
  updateCartWithAdditionalItem(name, id, parseInt(count), parseInt(price));
});

function updateCartWithAdditionalItem(name, id, count, price) {
  let item = document.getElementById(id + "Id");
  if (count === 0 && item) {
    item.remove();
  }
  if (!item && count > 0) {
    appendAdditionalItemToCart(name, id + "Id", count, price);
  }
  if (item && count > 0) {
    let totalPrice = count * price;
    item.querySelector(".cart-item__count").textContent = `(${count})`;
    item.querySelector(".cart-item__price").textContent = totalPrice;
  }
  calculateTotalPrice();
}

function appendAdditionalItemToCart(name, id, count, price) {
  let cart = document.getElementById("cart").querySelector(".cart__additional");
  cart.innerHTML += `
  <div id="${id}" class="cart__aditional-item mt-3">
    <div class="d-flex justify-content-between">
      <b>${name} <span class="cart-item__count">(${count})</span></b>
      <span class="text--gray">
        <span class="cart-item__price">
          ${count * price}
        </span> 
        USD
      </span>
    </div>
  </div>
  `;
}

function calculateTotalPrice() {
  let priceNodes = document.querySelectorAll(".cart-item__price");
  let sum = 0;
  priceNodes.forEach((node) => {
    sum += parseInt(node.textContent);
  });
  document.getElementById("carTotalPrice").textContent = sum;
}

var returnTripWatcher = {
  showReturnTrip: false,
  listener: function (value) {},
  set value(value) {
    (this.showReturnTrip = value), this.listener(value);
  },
  get() {
    return this.showReturnTrip;
  },
  registerListener: function (listener) {
    this.listener = listener;
  },
};

returnTripWatcher.registerListener(onReturnTripToggle);

function onReturnTripToggle(toggleValue) {
  changePrices(toggleValue, ".additional-item", ".additional-item__hidden");
  changePrices(toggleValue, ".cart__aditional-item", ".cart-item__price");
  calculateTotalPrice();
}

function changePrices(toggleValue, itemsClass, priceClass) {
  document.querySelectorAll(itemsClass).forEach((item) => {
    let priceElement = item.querySelector(priceClass);
    let price = parseInt(priceElement.textContent);
    if (toggleValue) {
      priceElement.textContent = price * 2;
    } else {
      priceElement.textContent = price / 2;
    }
  });
}
